use iced::{Align, Application, Checkbox, Clipboard, Column, Command, Container, Element, HorizontalAlignment, Length, PickList, Row, Rule, Scrollable, Settings, Space, Text, TextInput, button, executor, pick_list, scrollable, text_input, window};
use std::{
    cmp::Ordering,
    collections::BTreeSet,
    fmt,
    path::{Component::RootDir, Path, PathBuf},
    time::SystemTime,
};

mod history;
mod prefs;
mod style;
mod select;

use prefs::*;
use history::*;
use select::*;

pub type Error = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, Error>;

const DEFAULT_FONT: &[u8] = include_bytes!("resources/Lato-Regular.ttf");

fn main() -> Result<()> {
    let window = window::Settings {
        icon: None,
        ..Default::default()
    };

    let settings = Settings {
        flags: (),
        antialiasing: true,
        default_font: Some(DEFAULT_FONT),
        default_text_size: 18,
        exit_on_close_request: true,
        window,
    };

    Ok(Browser::run(settings)?)
}

pub struct Browser {
    mode: Mode,
    prefs: Preferences,
    pwd_entries: Vec<FileView>,
    selected: BTreeSet<PathBuf>,
    history: History,
    location_state: text_input::State,
    location_text: String,
    scrollable: scrollable::State,
    parent_button: button::State,
    hist_fwd_button: button::State,
    hist_back_button: button::State,
    mkdir_button: button::State,
    prefs_button: button::State,
    hist_list: pick_list::State<PickPath>,
    home_button: button::State,
    components_list: pick_list::State<PickPath>,
    error: Option<String>,
}

impl Default for Browser {
    fn default() -> Self {
        let pwd = home_dir();
        let prefs = Preferences::from_user().unwrap_or_default();

        Browser {
            mode: Mode::Main,
            prefs,
            selected: Default::default(),
            location_state: text_input::State::focused(),
            location_text: pwd.display().to_string(),
            pwd_entries: Default::default(),
            scrollable: Default::default(),
            parent_button: Default::default(),
            hist_fwd_button: Default::default(),
            hist_back_button: Default::default(),
            mkdir_button: Default::default(),
            hist_list: Default::default(),
            home_button: Default::default(),
            prefs_button: Default::default(),
            history: History::new(pwd),
            components_list: Default::default(),
            error: Default::default(),
        }.refilled()
    }
}

impl Browser {
    fn set_mode(&mut self, mode: Mode) {
        match mode {
            Mode::Mkdir => self.location_text = String::new(),
            Mode::Main => self.location_text = self.pwd().display().to_string(),
            Mode::Prefs => (),
        }
        self.mode = mode;
    }

    fn update_prefs(&mut self, prefs: PrefMessage) {
        self.prefs.update(prefs);
        self.refill_entries().unwrap_or_default();
    }

    fn pwd(&self) -> &Path {
        self.history.current()
    }

    fn sort(&mut self) {
        let Browser { pwd_entries, prefs, .. } = self;
        pwd_entries.sort_by(|a, b| {
            match prefs.sorting {
                Sorting::AlphaNum { case_sensitive } => if case_sensitive {
                    a.file_name().cmp(&b.file_name())
                } else {
                    a.file_name().to_lowercase().cmp(&b.file_name().to_lowercase())
                },
                Sorting::Natural => {
                    a.file_name_natural().cmp(&b.file_name_natural())
                }
                Sorting::ByDate => {
                    b.mod_date().cmp(&a.mod_date())
                },
            }
        });

        if prefs.sort_reverse {
            self.pwd_entries.reverse();
        }

        if prefs.dirs_first {
            self.pwd_entries.sort_by(|a, b| match (a.file_type(), b.file_type()) {
                (FileType::Directory, FileType::Directory) => Ordering::Equal,
                (FileType::Directory, _) => Ordering::Less,
                (_, _) => Ordering::Greater,
            });
        }
    }

    fn refill_entries(&mut self) -> Result<()> {
        self.pwd_entries = self.pwd().read_dir()?
            .filter_map(|rd| Some(FileView::from(rd.ok()?)))
            .filter(|file_view| {
                if self.prefs.show_hidden {
                    true
                } else {
                    !file_view.is_hidden()
                }
            })
            .collect();

        self.location_text = self.pwd().display().to_string();
        self.location_state.move_cursor_to_end();
        self.sort();
        self.error = None;
        eprintln!("{}", self.pwd().display());
        Ok(())
    }

    fn refilled(mut self) -> Self {
        self.refill_entries().unwrap_or_default();
        self
    }

    fn can_parent(&self) -> bool {
        self.pwd().parent().is_some()
    }

    fn parent(&mut self) {
        if let Some(parent) = self.pwd().parent() {
            let parent = parent.to_path_buf();
            self.cd(parent);
        }
    }

    fn cd<P: AsRef<Path>>(&mut self, path: P) {
        if let Ok(path) = path.as_ref().canonicalize() {
            if path.is_dir() {
                self.history.push(path);
                self.refill_entries().unwrap_or_default();
            }
        }
    }

    fn hist_back(&mut self) {
        self.history.go_back();
        self.refill_entries().unwrap_or_default();
    }

    fn hist_fwd(&mut self) {
        self.history.go_fwd();
        self.refill_entries().unwrap_or_default();
    }

    fn components(&self) -> Vec<PickPath> {
        let mut components = Vec::new();
        let comps = self.pwd().components().map(|c| c.as_os_str()).collect::<Vec<_>>();
        for i in 0..comps.len() {
            let mut path = PathBuf::new();
            let comp = &comps[0..=i];
            for c in comp {
                path.push(c);
            }
            components.push(PickPath::from(path));
        }
        components.reverse();
        components
    }

    fn view_main(&mut self) -> Element<Message> {
        let can_parent = self.can_parent();
        let can_fwd = self.history.can_go_fwd();
        let can_back = self.history.can_go_back();
        let components = self.components();
        let last_component = Some(self.pwd().into());
        let view_mode = self.prefs.view_mode;

        let Browser {
            home_button,
            parent_button,
            hist_back_button,
            hist_fwd_button,
            prefs_button,
            hist_list,
            mkdir_button,
            location_state,
            components_list,
            pwd_entries,
            ..
        } = self;

        let home_button = style::nav_button(
            home_button,
            true,
            Message::Cd(home_dir())
        );

        let parent_button = style::nav_button(
            parent_button,
            can_parent,
            Message::Parent,
        );

        let back_button = style::nav_button(
            hist_back_button,
            can_back,
            Message::HistBack,
        );

        let fwd_button = style::nav_button(
            hist_fwd_button,
            can_fwd,
            Message::HistFwd,
        );

        let mkdir_button = style::nav_button(
            mkdir_button,
            true,
            Message::Mode(Mode::Mkdir),
        );

        let prefs_button = style::nav_button(
            prefs_button,
            true,
            Message::Mode(Mode::Prefs),
        );

        let nav_buttons = Row::new()
            .align_items(Align::End)
            .spacing(5)
            .push(home_button)
            .push(parent_button)
            .push(back_button)
            .push(fwd_button)
            .push(mkdir_button)
            .push(prefs_button);

        let location_input = TextInput::new(
            location_state,
            "/path/to/directory",
            &self.location_text,
            Message::UpdateLocText,
        ).padding(10)
        .on_submit(Message::UpdateLoc(self.location_text.clone()));

        let components = PickList::new(
            components_list,
            components,
            last_component,
            |path| Message::Cd(path.path)
        ).width(Length::Fill);

        let loc_col = Column::new()
            .spacing(5)
            .push(location_input)
            .push(components)
            .width(Length::Fill);

        let hist_list = PickList::new(
            hist_list,
            self.history.pick_list(),
            None,
            |path| Message::Cd(path.path)
        ).width(Length::Fill);

        let nav_col = Column::new()
            .spacing(5)
            .push(nav_buttons)
            .push(hist_list)
            .width(Length::Units(275));

        let nav = Row::new()
            .padding(10)
            .spacing(5)
            .push(loc_col)
            .push(nav_col);

        let entries = pwd_entries.iter_mut()
            .fold(Column::new().padding(10).spacing(5), |col, entry| {
                col.push(entry.view(view_mode))
            });

        let scrollable = Scrollable::new(&mut self.scrollable)
            .width(Length::Fill)
            .height(Length::Fill)
            .push(entries);

        let content = Column::new()
            .push(nav)
            .push(scrollable);

        Container::new(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }

    fn view_mkdir(&mut self) -> Element<Message> {
        let current = self.pwd().display().to_string();

        let text = Text::new("Create new directory");

        let input = TextInput::new(
            &mut self.location_state,
            &format!("New directory in {}", current),
            &self.location_text,
            Message::UpdateLocText,
        )
            .width(Length::Fill)
            .padding(10)
            .on_submit(Message::Mkdir(self.location_text.clone()));

        let cancel = style::button(&mut self.mkdir_button, "Cancel")
            .on_press(Message::Mode(Mode::Main));

        let cancel_row = Row::new()
            .push(Space::with_width(Length::Fill))
            .push(cancel);

        let mut content = Column::new()
            .align_items(Align::Center)
            .spacing(20)
            .padding(100)
            .push(text)
            .push(input)
            .push(cancel_row);

        if let Some(err) = self.error.as_ref() {
            content = content.push(Text::new(err));
        }

        Container::new(content)
            .align_x(Align::Center)
            .align_y(Align::Center)
            .into()
    }

    fn mkdir(&mut self, dir: String) -> Result<()> {
        let mut newdir = self.pwd().to_path_buf();
        newdir.push(dir);
        if let Err(e) = std::fs::create_dir(newdir) {
            self.error = Some(e.to_string());
        } else {
            self.mode = Mode::Main;
            self.refill_entries()?;
        }
        Ok(())
    }

    fn view_prefs(&mut self) -> Element<Message> {
        let Browser {
            prefs,
            ..
        } = self;

        let title = Text::new("Preferences")
            .size(32)
            .horizontal_alignment(HorizontalAlignment::Center);

        let prefs = prefs.view()
            .map(Message::SetPrefs);

        let done = style::button(&mut self.prefs_button, "Done")
            .on_press(Message::Mode(Mode::Main));

        let done_row = Row::new()
            .push(Space::with_width(Length::Fill))
            .push(done);

        let content = Column::new()
            .spacing(10)
            .push(title)
            .push(Rule::horizontal(10))
            .push(prefs)
            .push(done_row);

        Container::new(content)
            .align_x(Align::Center)
            .padding(100)
            .into()
    }
}

#[derive(Debug, Clone)]
pub enum Message {
    Select,
    Parent,
    Cd(PathBuf),
    HistBack,
    HistFwd,
    UpdateLoc(String),
    UpdateLocText(String),
    Mkdir(String),
    Mode(Mode),
    SetPrefs(PrefMessage),
}

impl Application for Browser {
    type Message = Message;
    type Flags = ();
    type Executor = executor::Default;

    fn new(flags: Self::Flags) -> (Self, Command<Message>) {
        (Self::default(), Command::none())
    }

    fn title(&self) -> String {
        format!("{} - {}", env!("CARGO_BIN_NAME"), self.pwd().display())
    }

    fn update(&mut self, message: Message, _clipboard: &mut Clipboard) -> Command<Message>{
        match message {
            Message::Select => (),
            Message::Parent => self.parent(),
            Message::HistBack => self.hist_back(),
            Message::HistFwd => self.hist_fwd(),
            Message::Cd(path) => self.cd(path),
            Message::UpdateLoc(s) => self.cd(s),
            Message::UpdateLocText(s) => self.location_text = s,
            Message::Mkdir(dir) => self.mkdir(dir).unwrap_or_default(),
            Message::Mode(mode) => self.set_mode(mode),
            Message::SetPrefs(prefs) => self.update_prefs(prefs),
        }

        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        match self.mode {
            Mode::Main => self.view_main(),
            Mode::Mkdir => self.view_mkdir(),
            Mode::Prefs => self.view_prefs(),
        }
    }

}

fn home_dir() -> PathBuf {
    directories::BaseDirs::new()
        .expect("no home directory")
        .home_dir()
        .to_path_buf()
}

#[derive(Debug, Clone, Copy)]
pub enum Mode {
    Main,
    Mkdir,
    Prefs,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PickPath {
    path: PathBuf,
    display: String,
}

impl PickPath {
    pub fn file_name(&self) -> String {
        self.path
            .file_name()
            .unwrap_or_else(|| RootDir.as_os_str())
            .to_string_lossy()
            .to_string()
    }

    pub fn file_name_natural(&self) -> String {
        let c: &'static [char] = &['.', '_', ' '];
        self.file_name()
            .to_lowercase()
            .trim_start_matches(c)
            .to_string()
    }

    pub fn mod_date(&self) -> SystemTime {
        self.path
            .metadata()
            .unwrap_or_else(|_| self.path.symlink_metadata().unwrap())
            .modified()
            .unwrap_or(SystemTime::UNIX_EPOCH)
    }
}

impl fmt::Display for PickPath {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.display)
    }
}

impl<P: AsRef<Path>> From<P> for PickPath {
    fn from(path: P) -> Self {
        PickPath {
            display: path.as_ref().display().to_string(),
            path: path.as_ref().to_path_buf(),
        }
    }
}
