use super::*;
use iced::svg::{Handle, Svg};
use std::fs::{DirEntry};

const ICON_DIR: &[u8] = include_bytes!("resources/icon_dir.svg");
const ICON_DIR_LINK: &[u8] = include_bytes!("resources/icon_dir_link.svg");
const ICON_FILE: &[u8] = include_bytes!("resources/icon_file.svg");
const ICON_FILE_LINK: &[u8] = include_bytes!("resources/icon_file_link.svg");
// const ICON_LINK: &[u8] = include_bytes!("resources/icon_symlink.svg");

const HIDDEN_CHARS: &[char] = &['.', '_', ' '];

#[derive(Debug, Copy, Clone)]
pub enum FileType {
    Directory,
    File,
}

pub struct FileView {
    entry: DirEntry,
    file_type: FileType,
    is_symlink: bool,
    icon_handle: Handle,
}

impl FileView {
    pub fn file_name(&self) -> String {
        self.entry.file_name()
            .to_string_lossy()
            .to_string()
    }

    pub fn file_name_natural(&self) -> String {
        self.file_name()
            .to_lowercase()
            .trim_start_matches(HIDDEN_CHARS)
            .to_string()
    }

    pub fn mod_date(&self) -> SystemTime {
        self.entry
            .metadata()
            .unwrap_or_else(|_| self.entry.path().symlink_metadata().unwrap())
            .modified()
            .unwrap_or(SystemTime::UNIX_EPOCH)
    }

    pub fn file_type(&self) -> FileType {
        self.file_type
    }

    pub fn is_hidden(&self) -> bool {
        self.file_name()
            .starts_with(HIDDEN_CHARS)
    }

    pub fn handled(mut self) -> Self {
        let icon = match (self.file_type, self.is_symlink) {
            (FileType::File, false) => ICON_FILE,
            (FileType::File, true) => ICON_FILE_LINK,
            (FileType::Directory, false) => ICON_DIR,
            (FileType::Directory, true) => ICON_DIR_LINK,
        };
        self.icon_handle = Handle::from_memory(icon);
        self
    }

    pub fn view(&mut self, view_mode: ViewMode) -> Element<Message> {
        match view_mode {
            ViewMode::Basic => {
                Row::new()
                    .spacing(5)
                    .push(Svg::new(self.icon_handle.clone()).height(Length::Units(20)))
                    .push(Text::new(self.file_name()))
                    .into()
            }
            ViewMode::Details => {
                Row::new()
                    .spacing(5)
                    .push(Svg::new(self.icon_handle.clone()).height(Length::Units(20)))
                    .push(Text::new(self.file_name()))
                    .push(Text::new("DATE"))
                    .into()
            }
        }
    }
}

impl From<DirEntry> for FileView {
    fn from(entry: DirEntry) -> Self {
        let file_type = entry.file_type().expect("file_type");
        let mut is_symlink = false;

        let file_type = if file_type.is_dir() {
            FileType::Directory
        } else if file_type.is_symlink() {
            is_symlink = true;
            FileType::from(entry.path().read_link().expect("cannot read_link"))
        } else {
            FileType::File
        };

        let icon_handle: &[u8] = &[];

        FileView {
            icon_handle: Handle::from_memory(icon_handle),
            file_type,
            entry,
            is_symlink,
        }.handled()
    }
}

impl<P: AsRef<Path>> From<P> for FileType {
    fn from(path: P) -> Self {
        let path = path.as_ref();
        if let Ok(readlink) = path.read_link() {
            FileType::from(readlink)
        } else {
            if let Ok(metadata) = path.metadata() {
                if metadata.file_type().is_dir() {
                    FileType::Directory
                } else {
                    FileType::File
                }
            } else {
                FileType::File
            }
        }
    }
}

impl fmt::Debug for FileView {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.entry.path().display())
    }
}
