use super::*;
use directories::BaseDirs;
use iced::{Radio, Rule};
use std::{fmt, fs::{File, create_dir_all}, io::{BufReader, BufWriter}};
use serde::*;

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Preferences {
    pub sorting: Sorting,
    pub sort_reverse: bool,
    pub show_hidden: bool,
    pub dirs_first: bool,
    pub view_mode: ViewMode,
}

impl Default for Preferences {
    fn default() -> Self {
        Preferences {
            sorting: Sorting::Natural,
            sort_reverse: false,
            show_hidden: false,
            dirs_first: false,
            view_mode: ViewMode::Basic,
        }
    }
}

#[test]
fn serde_prefs() {
    let prefs0 = Preferences::default();
    let prefs_json0 = serde_json::to_string_pretty(&prefs0).unwrap();
    println!("{}", prefs_json0);

    let prefs_json1 = r#"{
        "sorting": "Natural",
        "sort_reverse": false,
        "show_hidden": false,
        "dirs_first": false,
        "view_mode": "Basic"
    }"#;
    let prefs1: Preferences = serde_json::from_str(prefs_json1).unwrap();
    dbg!(&prefs1);
    assert_eq!(prefs1, Preferences::default());
    assert_eq!(prefs0, prefs1);

    let mut prefs2 = Preferences::default();
    prefs2.sorting = Sorting::AlphaNum{ case_sensitive: true };
    println!("{}", serde_json::to_string_pretty(&prefs2).unwrap());

    let prefs_json2 = r#"{
        "sorting": {
            "AlphaNum": {
                "case_sensitive": true
            }
        },
        "sort_reverse": true,
        "show_hidden": true,
        "dirs_first": true,
        "view_mode": "Details"
    }"#;
    let prefs = serde_json::from_str::<Preferences>(prefs_json2).unwrap();
    dbg!(prefs);
}

pub fn config_file() -> Option<PathBuf> {
    Some(
        BaseDirs::new()?
            .preference_dir()
            .to_path_buf()
            .join(format!("{}.json", env!("CARGO_BIN_NAME"))),
    )
}

impl Preferences {
    pub fn from_user() -> Option<Self> {
        let config_file = config_file()?;
        let reader = BufReader::new(File::open(config_file).ok()?);
        serde_json::from_reader(reader).ok()
    }

    pub fn update(&mut self, msg: PrefMessage) {
        match msg {
            PrefMessage::ShowHidden(show) => self.show_hidden = show,
            PrefMessage::ReverseSort(rev) => self.sort_reverse = rev,
            PrefMessage::ChangeSort(sorting) => self.sorting = sorting.to_owned(),
            PrefMessage::DirsFirst(first) => self.dirs_first = first,
            PrefMessage::ChangeViewMode(view) => self.view_mode = view,
        }

        if let Some(config_file) = config_file() {
            match self.write_to_file(&config_file) {
                Ok(()) => eprintln!("Saving preferences: `{}`", config_file.display()),
                Err(e) =>  eprintln!("{}", e),
            }
        }
    }

    pub fn write_to_file<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        if let Some(dir) = path.as_ref().parent() {
            if !dir.exists() {
                create_dir_all(dir)?;
            }
        }
        let writer = BufWriter::new(File::create(path)?);
        serde_json::to_writer_pretty(writer, self)?;
        Ok(())
    }

    pub fn view(&mut self) -> Element<PrefMessage> {
        let sorting_title = Text::new("Sort:");
        let sort_rev = Checkbox::new(self.sort_reverse, "Reverse", PrefMessage::ReverseSort);

        let sort_header = Row::new()
            .spacing(10)
            .push(sorting_title)
            .push(sort_rev);

        let sortings = Sorting::ALL
            .iter()
            .fold(
                Column::new().spacing(10),
                |column, sorting| {
                    column.push(sorting.view(self.sorting))
                }
            );

        let view_header = Row::new()
            .spacing(10)
            .push(Text::new("View Mode"));

        let view_modes = ViewMode::ALL
            .iter()
            .fold(
                Column::new().spacing(10),
                |column, view_mode| {
                    column.push(view_mode.view(self.view_mode))
                }
            );

        let show_hidden = Checkbox::new(self.show_hidden, "Show hidden files", PrefMessage::ShowHidden);
        let dirs_first = Checkbox::new(self.dirs_first, "Directories first", PrefMessage::DirsFirst);

        Column::new()
            .spacing(10)
            .push(sort_header)
            .push(sortings)
            .push(Rule::horizontal(10))
            .push(view_header)
            .push(view_modes)
            .push(Rule::horizontal(10))
            .push(show_hidden)
            .push(dirs_first)
            .into()
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum PrefMessage {
    ShowHidden(bool),
    ReverseSort(bool),
    ChangeSort(Sorting),
    DirsFirst(bool),
    ChangeViewMode(ViewMode),
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Serialize, Deserialize)]
pub enum Sorting {
    AlphaNum {
        case_sensitive: bool,
    },
    Natural,
    ByDate,
}

impl Sorting {
    const ALL: &'static [Self] = &[
        Sorting::AlphaNum { case_sensitive: true },
        Sorting::AlphaNum { case_sensitive: false },
        Sorting::Natural,
        Sorting::ByDate,
    ];

    fn view(&self, selected: Self ) -> Element<PrefMessage> {
        Radio::new(
            *self,
            self.to_string(),
            Some(selected),
            PrefMessage::ChangeSort,
        ).into()
    }
}

impl Default for Sorting {
    fn default() -> Self {
        Sorting::Natural
    }
}

impl fmt::Display for Sorting {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Sorting::AlphaNum { case_sensitive } => if *case_sensitive {
                "AlphaNumeric (case sensitive)"
            } else {
                "AlphaNumeric (case insensitive)"
            },
            Sorting::Natural => "Natural",
            Sorting::ByDate => "Date modified",
        })
    }       
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ViewMode {
    Basic,
    Details,
}

impl ViewMode {
    pub const ALL: &'static [Self] = &[ViewMode::Basic, ViewMode::Details];

    fn view(&self, selected: Self) -> Element<PrefMessage> {
        Radio::new(
            *self,
            self.to_string(),
            Some(selected),
            PrefMessage::ChangeViewMode,
        ).into()
    }
}

impl fmt::Display for ViewMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
