use super::*;
use iced::{
    button, Background, Color, HorizontalAlignment, Text, Vector,
};

pub enum NavButton {
    Primary,
    Secondary,
}

impl button::StyleSheet for NavButton {
    fn active(&self) -> button::Style {
        button::Style {
            background: Some(Background::Color(match self {
                NavButton::Primary => Color::from_rgb(0.11, 0.42, 0.87),
                NavButton::Secondary => Color::from_rgb(0.5, 0.5, 0.5),
            })),
            border_radius: 12.0,
            shadow_offset: Vector::new(1.0, 1.0),
            text_color: Color::from_rgb8(0xEE, 0xEE, 0xEE),
            ..button::Style::default()
        }
    }

    fn hovered(&self) -> button::Style {
        button::Style {
            text_color: Color::WHITE,
            shadow_offset: Vector::new(1.0, 2.0),
            ..self.active()
        }
    }
}

pub(crate) fn button<T: Into<String>>(state: &mut button::State, label: T) -> iced::Button<Message> {
    iced::Button::new(
        state,
        Text::new(label).horizontal_alignment(HorizontalAlignment::Center),
    )
    .padding(10)
    .min_width(40)
}

pub(crate) fn nav_button(
    state: &mut button::State,
    enabled: bool,
    message: Message,
) -> iced::Button<Message> {
    let text = match message {
        Message::Parent => "↑",
        Message::HistBack => "←",
        Message::HistFwd => "→",
        Message::Cd(_) => "⌂",
        Message::Mode(Mode::Mkdir) => "+",
        Message::Mode(Mode::Prefs) => "⁞",
        _ => unreachable!("nav_button"),
    };

    if enabled {
        button(state, text)
            .style(NavButton::Primary)
            .on_press(message)
    } else {
        button(state, text)
            .style(NavButton::Secondary)
    }
}

pub enum LineItem<'a> {
    Selected(&'a PickPath),
    Unselected(&'a PickPath),
}

impl<'a> button::StyleSheet for LineItem<'a> {
    fn active(&self) -> button::Style { 
        match self {
            LineItem::Selected(_) => NavButton::Primary.active(),
            LineItem::Unselected(_) => NavButton::Primary.hovered(),
        }
    }
}
