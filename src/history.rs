use std::path::{Path, PathBuf};
use crate::PickPath;

#[derive(Debug)]
pub struct History {
    inner: Vec<PathBuf>,
    current: usize,
}

impl History {
    pub fn new(path: PathBuf) -> Self {
        History {
            inner: vec![path],
            current: 0,
        }
    }

    pub fn push(&mut self, path: PathBuf) {
        if self.current() != path {
            let (first, _last) = self.inner.split_at(self.current + 1);
            self.inner = first.to_vec();
            self.inner.push(path);
            self.current += 1;
        }
    }

    pub fn current(&self) -> &Path {
        self.inner[self.current].as_path()
    }

    pub fn can_go_fwd(&self) -> bool {
        self.inner.len() - self.current > 1
    }

    pub fn can_go_back(&self) -> bool {
        self.current > 0
    }

    pub fn go_fwd(&mut self) -> Option<&Path> {
        if self.can_go_fwd() {
            self.current += 1;
            Some(self.current())
        } else {
            None
        }
    }

    pub fn go_back(&mut self) -> Option<&Path> {
        if self.can_go_back() {
            self.current -= 1;
            Some(self.current())
        } else {
            None
        }
    }

    pub fn pick_list(&self) -> Vec<PickPath> {
        self.inner.iter().map(|path| path.into()).collect()
    }
}

#[test]
fn history() {
    let mut hist = History::new("a".into());
    assert!(!hist.can_go_fwd());
    assert!(!hist.can_go_back());

    hist.push("b".into());
    assert!(!hist.can_go_fwd());
    assert!(hist.can_go_back());

    hist.go_back();
    assert!(hist.can_go_fwd());
    assert!(!hist.can_go_back());

    hist.go_fwd();
    assert!(!hist.can_go_fwd());
    assert!(hist.can_go_back());

    for i in &["c", "d", "e"] {
        hist.push(i.into());
    }

    for _ in 0..hist.inner.len() - 1 {
        assert!(hist.go_back().is_some());
    }
    assert!(hist.go_back().is_none());

    hist.push("f".into());
    assert_eq!(hist.current(), Path::new("f"));
    assert!(!hist.can_go_fwd());
}
